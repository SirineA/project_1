/**
 * @author Sirine Aoudj
 */
package movies.importer;

import java.util.ArrayList;

public class ImbdImporter extends Processor{
	
	/**
	 * @param source is the folder where we are taking the .txt file that contains the imbd movies
	 * @param output is the folder where we want to add the processed imbd file
	 * @param srcContaainsHeader is whether we keep the header in the file
	 * This constructor create a processor imbdImporter for the imbd file
	 *
	 */
	public ImbdImporter(String source, String output, boolean srcContainsHeader) {
		super(source, output, true);
	}
	
	/**
	 * This process method will return an array that contains all the valid movie strings that are valid  
	 *@param input is the arrayList<string> thats contains all the lines of the imbd.txt file with the info of the movies  non filtered
	 *@return imbdmovies is the arrayList<string> that will contain the movies info in the format --> Name: 	Release Year: 	Runtime: 	Source: Imbd
	 **@return is the arrayList<String> imbdMovies that contains all the validated lines
	 */
	public ArrayList<String> process(ArrayList<String> moviesList){
		
		//First we are creating an a new String array to store the new line that only contains the info we want to retrieve
		ArrayList<String> imbdMovies = new ArrayList<String>();
		
		/*CORRECTLENGTHT is the correct number of columns per line in the original imbd file, it should be 21 as there are 21 type of information in the heading
		  int NAMECOLUMN,YEARCOLUMN,RUNTIMECOLUMN is the index in the string array at which we want to retrieve the name, year and runtime*/
		 final int CORRECTLENGTH = 20;
		 final int NAMECOLUMN = 1;
		 final int YEARCOLUMN = 3;
		 final int RUNTIMECOLUMN = 6;
		
		//then we set loop that goes through each lines of the input, if its the first line we continue to not retrieve anything since it is a header line
		for(int i = 1; i < moviesList.size(); i++) {
			
			//String movieLineInfo is the line in the original file that we are taking the value from
			String movieInfoLine = moviesList.get(i);
			
			//then we set empty strings for each information we want to retrieve
			String name = "";
			String releaseYear = "";
			String runtime = "";
			
			//Then we split the current line string at the tabs and store that into an array of strings where each column corresponds to an index of the array
			String[] splitInput = movieInfoLine.split("\t",-1) ;
			
			//Then we verify the length of the array if it does not contains the right number of column we continue the loop 
			//as we do no want any line that do not have the necessary information
			if(splitInput.length < CORRECTLENGTH) {
				continue;
			}
			
			//Then we take from the array the information we want and add them to their respective strings
			name = splitInput[NAMECOLUMN];
			releaseYear = splitInput[YEARCOLUMN];
			runtime = splitInput[RUNTIMECOLUMN];
			
			//We then create a new movie object using these informations we have retrieved from the current line and set the source to imbd
			Movie movie = new Movie(name, releaseYear, runtime,"IMBD");
			
			//We then make it into a string then finally add it to the new arrayList<string> imbdMovies 
			imbdMovies.add(movie.toString());
			
			//This line calls the method printProgress that we defined in the movie class that allows us to print the progress of which movie we are working on in a percentage form
			printProgress(moviesList, i, "IMBD Importer: ");
		}
		//returning the new arrayList<String>
		return imbdMovies;
	}

}
