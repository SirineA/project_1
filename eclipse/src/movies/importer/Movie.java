/**
 * @author David Nguyen and Sirine Aoudj
 */
package movies.importer;

/**Creating a movie Object
 * @param String name is the name of the movie object
 * @param String releaseYear is the release year of the movie object
 * @param String runtime is the runtime of the movie object
 * @param String source is the source of the movie object (imbd,kaggle)
 */

public class Movie {	
	private String name = "";
	private String releaseYear = "";
	private String runtime = "";
	private String source = "";
	
	/**
	 * This is the movie constructor
	 * @param name is the name of the movie
	 * @param releaseYear is the release year of the movie
	 * @param runtime is the runtime of the movie
	 * @param source is the source of the movie
	 */
	public Movie(String name, String releaseYear, String runtime, String source) {
		this.name = name;
		this.releaseYear = releaseYear;
		this.runtime = runtime;
		this.source = source;
	}
	
	/**
	 * This is to get the name of a movie object
	 * @return the movie name
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * This is to get the release year of a movie object
	 * @return the movie releaserYear
	 */
	public String getReleaseYear() {
		return this.releaseYear;
	}
	
	/**
	 * This is to get the runtime of a movie object
	 * @return the movie runtime
	 */
	public String getRuntime() {
		return this.runtime;
	}
	
	/**
	 * This is to get the source of a movie object
	 * @return the movie source
	 */
	public String getSource() {
		return this.source;
	}
	
	/**
	 * This is to set the name of a movie object
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * This is to set the releaseYear of a movie object
	 * @param releaseYear
	 */
	public void setReleaseYear(String releaseYear) {
		this.releaseYear = releaseYear;
	}
	
	/**
	 * This is to set the runtime of a movie object
	 * @param runtime
	 */
	public void setRuntime(String runtime) {
		this.runtime = runtime;
	}
	
	/**
	 * This is to set the source of a movie object
	 * @param source
	 */
	public void setSource(String source) {
		this.source = source;
	}
	
	/**
	 * Overriding the toString method to print the movie object  in this format:
	 * Name: name	Release Year: releaseYear	Runtime: runtime	Source: source;
	 */
	public String toString() {
		return "Name: " + this.name + 
				"	Release Year: " + this.releaseYear + 
				"	Runtime: " + this.runtime + 
				"	Source: " + this.source;
	}
	
	/**
	 * equals is a method that verifies if the object given is equal to whatever is being compared
	 * @param o is the object given
	 */
	@Override
	public boolean equals(Object o) {
		//if o is not a movie, it should return false
		if(!(o instanceof Movie)) {
			return false;
		}
		//verifies if the movie has the same name and year, if not returns false
		//after it verifies if the difference in runtime is less or equal to 5, if not returns false
		//after it verifies if the source are different, if yes then the source will be merged, else it will stay the same
		if(((Movie)o).name.equals(this.name) && ((Movie)o).releaseYear.equals(this.releaseYear)){
			if(Math.abs(Integer.parseInt(((Movie)o).runtime) - Integer.parseInt(this.runtime)) <= 5 ) {
				if(!this.source.equals(((Movie)o).source)) {
					((Movie)o).setSource("kaggle:IMBD");
				}
				return true;
			}
		}
		return false;
		
	}
}
