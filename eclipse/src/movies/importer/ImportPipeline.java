/**
 * @author David Nguyen and Sirine Aoudj
 */
package movies.importer;

import java.io.File;
import java.io.IOException;

public class ImportPipeline {

	/**
	 * This is the main method of the program where we create the processors and call them on the movie files
	 * @throws IOException
	 * in the main application we set the path of the folders as string
	 * then we create a processor array that contains all the processors 
	 * then we create processor objects to go in the array
	 * finally we call the processAll method that will execute each of them in order.
	 */
	public static void main(String[] args) throws IOException{
		
		/*kaggleFolder is the path where the kaggle.txt is stored in
		 *imbdFolder is the path where the imbd.txt is stored in 
		 *processedFolder is the path where the processed,validated,normalized and deduped files will be*/
		String basePath = new File("").getAbsolutePath();
		String kaggleFolder = basePath.replace("eclipse", "movies_txt\\kaggle");
		String imbdFolder = basePath.replace("eclipse", "movies_txt\\imbd");
		String processedFolder = basePath.replace("eclipse", "movies_txt\\processed");
		
		/*Processor[] allProcessor is an array of all the processors that we need to execute*/
		Processor[] allProcessors = new Processor[5];
		allProcessors[0] = new KaggleImporter(kaggleFolder,processedFolder, true);
		allProcessors[1] = new ImbdImporter(imbdFolder,processedFolder, true);
		allProcessors[2] = new Normalizer(processedFolder, processedFolder, false);
		allProcessors[3] = new Validator(processedFolder, processedFolder, false);
		allProcessors[4] = new Deduper(processedFolder, processedFolder, false);
		
		processAll(allProcessors);
	}
	/**
	 * This method uses a loop to go through the array of processor and execute each of them
	 * @param allProcessors is the array of processors that we want to execute in the main application
	 * @throws IOException
	 */
	public static void processAll(Processor[] allProcessors) throws IOException {
		for(int i = 0; i < allProcessors.length; i++) {
			allProcessors[i].execute();
		}
	}

}
