
package movies.importer;

import java.util.ArrayList;
/**
 * KaggleImporter is a class that takes an ArrayList, converts each line into a standardized format, and returns the standardized ArrayList.
 * The ArrayList given as a parameter is usually under the Kaggle's format.
 * 
 * @author David Nguyen
 *
 */

public class KaggleImporter extends Processor{
	/**
	 * KaggleImporter constructor that calls the Processor constructor in order to make a KaggleImporter.
	 * @param srcDir is the source directory
	 * @param outputDir is the output directory
	 * @param srcContainsHeader determines whether the first line should be omitted or not
	 */
	public KaggleImporter(String srcDir, String outputDir, boolean srcContainsHeader) {
		super(srcDir, outputDir, true);
	}
	/**
	 * process from KaggleImporter is a method that standardized the given ArrayList
	 * @param input is an ArrayList that's going to get processed
	 */
	public ArrayList<String> process(ArrayList<String> input){
		ArrayList<String> standardizedList = new ArrayList<String>();
		final int BEGIN_RUNTIME = 13;
		final int BEGIN_TITLE = 15;
		final int BEGIN_YEAR = 20;
		final int NB_OF_ELEMENTS = 21;
		//Loops the input and extracts all information needed in order to make the standardized list.
		for(int i = 0; i < input.size(); i++) {
			String currentLine = input.get(i);
			//Splits the currentLine string into an array of strings.
			String[] currentLineArr = currentLine.split("\t",NB_OF_ELEMENTS);
			String runtime = "";
			String name = "";
			String year = "";
			
			//Loops throughout the whole string array and extracts all the information into variables.
			for(int a = 0; a < currentLineArr.length; a++) {
				//Extracts runtime
				if(a == BEGIN_RUNTIME) {
					runtime = currentLineArr[a];
				}
				//Extracts movie title
				if(a == BEGIN_TITLE) {
					name = currentLineArr[a];
				}
				//Extracts release year
				if(a == BEGIN_YEAR) {
					year = currentLineArr[a];
				}
			
			}
			
			Movie currentMovie = new Movie(name, year, runtime, "kaggle");
			standardizedList.add(currentMovie.toString());
			
			printProgress(input, i, "Kaggle Importer: ");
		}
		
		return standardizedList;
	}
}
