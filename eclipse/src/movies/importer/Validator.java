/**
 * @author Sirine Aoudj
 */
package movies.importer;

import java.util.ArrayList;

public class Validator extends Processor{
	
	/**
	 * @param source is the folder where we are taking the .txt file that contains the imbd movies after they have been through the imbd importer
	 * @param output is the folder where we want to add the validated imbd file
	 * @param srcContaainsHeader is whether we keep the header in the file in this case we do so it is set as false
	 * This constructor create a processor Validator for the imbd file
	 *
	 */
	public Validator(String source, String output, boolean srcContainsHeader) {
		super(source, output, false);
	}
	/**
	 *@param list is the arrayList<string> thats contains all the lines of the imbd.processed file that contains the right information of each movie
	 *@return validatedmovies is the arrayList<string> that will contain the movies info that are not containing any empty strings
	 */
	public ArrayList<String> process(ArrayList<String> list){
	
		//First we are creating an a new String array to store the new line that only contains the validated lines
		ArrayList<String> validatedMovies = new ArrayList<String>();
		
		/*int NAMECOLUMN,YEARCOLUMN,RUNTIMECOLUMN is the index in the string array at which we want to retrieve the name, year and runtime*/
		final int NAMECOLUMN = 0;
		final int YEARCOLUMN = 1;
		final int RUNTIMECOLUMN = 2;
		/*CORRECTLENGTHNAME,CORRECTLENGTHYEAR,CORRECTLENGTHRUNTIME is the length of the string name,year and runtime that should be at least at for it to contains information */
		final int CORRECTLENGTHNAME = 8;
		final int CORRECTLENGTHYEAR = 15;
		final int CORRECTLENGTHRUNTIME = 10;
		/*int INDEXOFYEAR,INDEXOFRUNTIME is the index of the string year that we want to retrieve the number of the year and runtime of the movie*/
		final int INDEXOFYEAR = 14;
		final int INDEXOFRUNTIME = 9;
		
		//then we set loop that goes through each lines of the list
		for(int i = 0; i < list.size(); i++) {
			
			/*String currentLine is the line in the original file that we are taking the value from*/
			String currentLine = list.get(i);
			
			/*Then we split the current line string at the tabs and store that into an array of strings where each column corresponds to an index of the array*/
			String[] currentLineArr = currentLine.split("\t");
			
			/*If the currentLine, at the name column has a length smaller then the correct length of which would contain a name 
			 * we continue and skip this iteration as this line is not valid*/
			 if(currentLineArr[NAMECOLUMN].length() < CORRECTLENGTHNAME) {
				 continue;
			 }
			/*else If the currentLine, at the release year column has a length smaller then the correct length of which would contain a release year 
			 * we continue and skip this iteration as this line is not valid*/
			 else if(currentLineArr[YEARCOLUMN].length() < CORRECTLENGTHYEAR) {
				 continue;
			 }
			/*else If the currentLine, at the runtime column has a length smaller then the correct length of which would contain a runtime 
			 * we continue and skip this iteration as this line is not valid*/
			else if(currentLineArr[RUNTIMECOLUMN].length() < CORRECTLENGTHRUNTIME) {
				 continue;
			 }
			 /*Here we retrieve the year from the current line then we substring it at the index where the number of the year is supposed to start 
			  * then we try to make it an int with parseInt, if it does not then we throw a NumberFormatException and skip this iteration as this is not a valid line*/
			 try {
				 /*String year is the number in the string yearInfo that we retrieved
				   String runtimeInfo is the string containing: Runtime : runtime , runtime being the release year*/
				 String yearInfo = currentLineArr[YEARCOLUMN];
				 String yearNumber = yearInfo.substring(INDEXOFYEAR);
				 Integer.parseInt(yearNumber);
				}
				catch(Exception NumberFormatException) {
				  continue;
				}
			/*Here we retrieve the runtime from the current line then we substring it at the index where the number of the runtime is supposed to start 
			 * then we try to make it an int with parseInt, if it does not then we throw a NumberFormatException and skip this iteration as this is not a valid line*/
			 try {
				 /*String year is the number in the string yearInfo that we retrieved
	 			   String runtimeInfo is the string containing: Runtime : runtime , runtime being the release year*/
				 String runtimeInfo = currentLineArr[RUNTIMECOLUMN];
				 String runtimeNumber = runtimeInfo.substring(INDEXOFRUNTIME);
				 Integer.parseInt(runtimeNumber);
				}
				catch(Exception NumberFormatException) {
				  continue;
				}
			 /*If the line passed all the conditions we add it to the validated movies*/
			 validatedMovies.add(list.get(i));

			 /*Then we call the print progress to print the percentage of which line it validating */
			 printProgress(list, i , "Validator: ");
		}
		//return the validated movies
		return validatedMovies;
	}

}
