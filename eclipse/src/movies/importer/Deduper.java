package movies.importer;

import java.util.ArrayList;

/**
 * The Deduper class verifies if there's any duplicates and removes the movie from the list.
 * @author David Nguyen and Sirine Aoudj
 *
 */
public class Deduper extends Processor{
	/**
	 * Deduper constructor calls the Processor constructor in order to make a Deduper object.
	 * @param sourceDir is the source directory
	 * @param outputDir is the output directory 
	 * @param srcContainsHeader determines whether the first line should be omitted or not
	 */
	public Deduper(String sourceDir, String outputDir, boolean srcContainsHeader) {
		super(sourceDir, outputDir, false);
	}
	/**
	 * process method of Deduper will verify the given ArrayList if there are any duplicates.
	 * @param input is an ArrayList
	 */
	public ArrayList<String> process(ArrayList<String> input){
		//movieList is a variable that will hold all the movies from the input
		//noDuplicates is a movie array that will store all movies without duplicates
		//noDuplicatesStr is a string array that will take in all the string representation of the movies in noDuplicates.
		ArrayList<Movie> movieList = new ArrayList<Movie>();
		ArrayList<Movie> noDuplicates = new ArrayList<Movie>();
		ArrayList<String> noDuplicatesStr = new ArrayList<String>();
		
		final int NAME_INDEX = 0;
		final int YEAR_INDEX = 1;
		final int RUNTIME_INDEX = 2;
		final int SOURCE_INDEX = 3;
		final int BEGIN_TITLE = 6;
		final int BEGIN_YEAR = 14;
		final int BEGIN_RUNTIME = 9;
		final int BEGIN_SOURCE = 8;
		final int NB_OF_ELEMENTS = 4;
		//loops through the whole input
		for(int i = 0; i < input.size(); i++) {
			String[] currentLineArr = new String[4];
			String currentLine = input.get(i);
			currentLineArr = currentLine.split("\t",NB_OF_ELEMENTS);
			//Retrieves all the information by using substring
			String title = currentLineArr[NAME_INDEX].substring(BEGIN_TITLE, currentLineArr[NAME_INDEX].length());
			String year = currentLineArr[YEAR_INDEX].substring(BEGIN_YEAR, currentLineArr[YEAR_INDEX].length());
			String runtime = currentLineArr[RUNTIME_INDEX].substring(BEGIN_RUNTIME, currentLineArr[RUNTIME_INDEX].length());
			String source = currentLineArr[SOURCE_INDEX].substring(BEGIN_SOURCE, currentLineArr[SOURCE_INDEX].length());	
			
			//Stores one movie at a time into movieList
			movieList.add( new Movie(title,year,runtime,source));
			printProgress(input, i, "Adding movies to check for duplicates: ");
		}
		
		//loops through the whole movieList
		for(int i = 0; i < movieList.size(); i++) {
			//if the moveList does not contain a duplicate, add it to the noDuplicates list
			if(!noDuplicates.contains(movieList.get(i))) {
				noDuplicates.add(movieList.get(i));
			}
			printProgressMovie(movieList, i, "Checking duplicates: ");
		}
		
		//loops through the whole noDuplicates list
		for(int i = 0; i < noDuplicates.size(); i++) {
			//converts each movie from noDuplicates into a string
			noDuplicatesStr.add(noDuplicates.get(i).toString());
			
			printProgressMovie(noDuplicates, i, "Adding movies to noDuplicates: ");
		}
		
		System.out.println("Processing completed.");
		return noDuplicatesStr;
		
	}
	
	/**
	 * printProgressMovie prints the current progress of process
	 * @param input is the ArrayList of movies
	 * @param currentNumber is the current movie its processing
	 * @param processorName is the name of the processor
	 */
	public void printProgressMovie(ArrayList<Movie> input, int currentNumber, String processorName) {
		double progress = Math.round(((double)currentNumber/input.size()*10000))/100.00;		
		System.out.println(processorName + progress + "%");
	}
}
