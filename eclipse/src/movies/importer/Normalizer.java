package movies.importer;

import java.util.ArrayList;

/**
 * Normalizer is a class that normalizes the ArrayList given.
 * @author David Nguyen
 *
 */
public class Normalizer extends Processor{
	/**
	 * Normalizer is a constructor that calls the Processor constructor in order to make a Normalizer object.
	 * @param sourceDir is source directory
	 * @param outputDir is output directory
	 * @param srcContainsHeader determines if the first line is omitted or not
	 */
	public Normalizer(String sourceDir, String outputDir, boolean srcContainsHeader) {
		super(sourceDir, outputDir, false);
	}
	/**
	 * process from Normalizer is a method that will normalize the ArrayList.
	 * @param input is an ArrayList that's going to be normalized
	 */
	public ArrayList<String> process(ArrayList<String> input){
		ArrayList<String> normalizedArr = new ArrayList<String>();
		final int NAME_INDEX = 0;
		final int YEAR_INDEX = 1;
		final int RUNTIME_INDEX = 2;
		final int SOURCE_INDEX = 3;
		final int BEGIN_TITLE = 6;
		final int BEGIN_YEAR = 14;
		final int BEGIN_RUNTIME = 9;
		final int BEGIN_SOURCE = 8;
		final int NB_OF_ELEMENTS = 4;
		//Loops through the whole input
		for(int i = 0; i < input.size(); i++) {
			String currentLine = input.get(i);
			String[] currentLineArr = currentLine.split("\t",NB_OF_ELEMENTS);
			
			//Retrieves the title and converts into lower case.
			String name = currentLineArr[NAME_INDEX].substring(BEGIN_TITLE, currentLineArr[NAME_INDEX].length());
			name = name.toLowerCase();	
			
			//Retrieves the year
			String year = currentLineArr[YEAR_INDEX].substring(BEGIN_YEAR,currentLineArr[YEAR_INDEX].length());
			
			//Retrieves the source
			String source = currentLineArr[SOURCE_INDEX].substring(BEGIN_SOURCE,currentLineArr[SOURCE_INDEX].length());
			
			//Retrieves the runtime and only keep the first number/word
			String runtime = "";
			//If the source is kaggle, it will search for the last index of " " and perform a substring.
			//Else it will find the length and perform a substring to get the runtime.
			if(source.equals("kaggle")) {
				runtime = currentLineArr[RUNTIME_INDEX].substring(BEGIN_RUNTIME,currentLineArr[RUNTIME_INDEX].lastIndexOf(" "));
			}else {
				runtime = currentLineArr[RUNTIME_INDEX].substring(BEGIN_RUNTIME,currentLineArr[RUNTIME_INDEX].length());
			}
			
			Movie currentMovie = new Movie(name, year, runtime, source);
			normalizedArr.add(currentMovie.toString());

			printProgress(input, i , "Normalizer: ");
		}		
		return normalizedArr;
	}
}
