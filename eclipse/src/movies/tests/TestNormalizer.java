package movies.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import movies.importer.*;

/**
 * testNormalizer tests all the Normalizer's methods
 * @author David Nguyen
 *
 */
class TestNormalizer {
	
	/**
	 * testProcess test the process method of the Normalizer. It hard code values into an ArrayList and verifies if the information has been properly normalized.
	 */
	@Test
	void testProcess() {
		Normalizer testProcessor = new Normalizer("","",true);
		ArrayList<String> testArr = new ArrayList<String>();
		testArr.add("Name: Title	Release Year: Year	Runtime: 200 minutes	Source: kaggle");
		testArr.add("Name: SOMETHING4321@$#	Release Year: Year2	Runtime: 30 hours	Source: imdb");
		testArr = testProcessor.process(testArr);
		assertEquals("Name: title	Release Year: Year	Runtime: 200	Source: kaggle",testArr.get(0));
		assertEquals("Name: something4321@$#	Release Year: Year2	Runtime: 30	Source: imdb",testArr.get(1));
	}

}
