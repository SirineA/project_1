package movies.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import movies.importer.Validator;

public class TestValidator {
	
@Test
	/*This test hard codes multiple line of movie some of which contain empty string and this confirm that it only takes the 6 of them that are valid lines 
	 * since there are 6 lines that do not contain empty info**/
	void testValidator() {
		Validator test = new Validator("","",false);
		ArrayList<String> list = new ArrayList<String>();
		String s1 = "Name: Den sorte dr�m	Release Year: 1911	Runtime: 53	Source: IMBD";
		String s2 = "Name: Cleopatra	Release Year: 1912	Runtime: 100	Source: IMBD";
		String s3 = "Name: L'Inferno	Release Year: 1911	Runtime: 68	Source: IMBD";
		String s4 = "Name: \"From the Manger to the Cross; or, Jesus of Nazareth\"	Release Year: 1912	Runtime: 60	Source: IMBD";
		String s5 = "Name: Madame DuBarry	Release Year: 1919	Runtime: 85	Source: IMBD";
		String s6 = "Name: Quo Vadis?	Release Year: 1913	Runtime: 120	Source: IMBD";
		String s7 = "Name:  	Release Year: 1906	Runtime: 70	Source: IMBD";
		String s8 = "Name: The Story of the Kelly Gang	Release Year:  	Runtime: 70	Source: IMBD";
		String s9 = "Name: The Story of the Kelly Gang	Release Year: 1906	Runtime:  	Source: IMBD";
		String s10 = "Name:  	Release Year:  	Runtime: 100	Source: IMBD";
		String s11 = "Name: L'Inferno	Release Year: 1911	Runtime:  	Source: IMBD";
		list.add(s1);
		list.add(s2);
		list.add(s3);
		list.add(s4);
		list.add(s5);
		list.add(s6);
		list.add(s7);
		list.add(s8);
		list.add(s9);
		list.add(s10);
		list.add(s11);
		ArrayList<String> newList = new ArrayList<String>();
		newList = test.process(list);
		for(int i = 0; i < list.size();i++) {
			System.out.println(list.get(i));
		}
		assertEquals(6,newList.size());

}
}
