package movies.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import movies.importer.*;

/**
 * TestKaggleImporter test the KaggleImporter's methods
 * @author David Nguyen
 *
 */
class TestKaggleImporter {
	
	/**
	 * testProcess test the process method of KaggleImporter. It hard codes values to an ArrayList and verifies if information has been extracted properly.
	 */
	@Test
	void testProcess() {
		KaggleImporter testProcessor = new KaggleImporter("","",true);
		ArrayList<String> testArr = new ArrayList<String>();
		testArr.add("Cast 1	Cast 2	Cast 3	Cast 4	Cast 5	Cast 6	Description	Director 1	Director 2	Director 3	Genre	Rating	Release Date	Runtime	Studio	Title	Writer 1	Writer 2	Writer 3	Writer 4	Year");
		testArr.add("Cast 1	Cast 2	Cast 3	Cast 4	Cast 5	Cast 6	Description	Director 1	Director 2	Director 3	Genre	Rating	Release Date	Runtime2	Studio	Title2	Writer 1	Writer 2	Writer 3	Writer 4	Year2");
		testArr = testProcessor.process(testArr);
		assertEquals("Name: Title	Release Year: Year	Runtime: Runtime	Source: kaggle",testArr.get(0));
		assertEquals("Name: Title2	Release Year: Year2	Runtime: Runtime2	Source: kaggle",testArr.get(1));
	}

}
