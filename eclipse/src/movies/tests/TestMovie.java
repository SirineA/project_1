package movies.tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import movies.importer.*;

class TestMovie {

	@Test
	void testGetMethods() {
		Movie test = new Movie("The Effects of Drug Abuse","1997","5 minutes","DrugCo");
		assertEquals("The Effects of Drug Abuse", test.getName());
		assertEquals("1997", test.getReleaseYear());
		assertEquals("5 minutes", test.getRuntime());
		assertEquals("DrugCo", test.getSource());
	}
	
	@Test
	void testSetMethods() {
		Movie test = new Movie("The Effects of Drug Abuse","1997","5 minutes","DrugCo");
		test.setName("Yakuza");
		test.setReleaseYear("800");
		test.setRuntime("5000 hours");
		test.setSource("Bob and Billy");
		
		assertEquals("Yakuza", test.getName());
		assertEquals("800", test.getReleaseYear());
		assertEquals("5000 hours", test.getRuntime());
		assertEquals("Bob and Billy", test.getSource());
	}
	
	@Test
	void testToString() {
		Movie test = new Movie("The Effects of Drug Abuse","1997","5 minutes","DrugCo");
		assertEquals("Name: The Effects of Drug Abuse	Release Year: 1997	Runtime: 5 minutes	Source: DrugCo",test.toString());
	}

}
