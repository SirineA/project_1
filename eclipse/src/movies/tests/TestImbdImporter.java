package movies.tests;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import movies.importer.*;
import java.util.ArrayList;

public class TestImbdImporter {
	
	/*This test hard codes multiple line of movie and we compare that to the one what the filtered string should look like 
	 *in this case it doesnt take the first line as expected and it output the right string for the other lines**/
	@Test
	void testImbdProcessor() {
	ArrayList<String> list = new ArrayList<String>();
	String s1 = "imdb_title_id	title	original_title	year	date_published	genre	duration	country	language	director	writer	production_company	actors	description	avg_vote	votes	budget	usa_gross_income";
	String s2 = "tt0000009	Miss Jerry	Miss Jerry	1894	1894-10-09	Romance	45	USA	None	Alexander Black	Alexander Black	Alexander Black Photoplays	\"Blanche Bayliss, William Courtenay, Chauncey Depew\"	The adventures of a female reporter in the 1890s.	5.9	154					1	2";
	String s3 = "tt0000574	The Story of the Kelly Gang	The Story of the Kelly Gang	1906	12/26/1906	\"Biography, Crime, Drama\"	70	Australia	None	Charles Tait	Charles Tait	J. and N. Tait	\"Elizabeth Tait, John Tait, Norman Campbell, Bella Cola, Will Coyne, Sam Crewes, Jack Ennis, John Forde, Vera Linden, Mr. Marshall, Mr. McKenzie, Frank Mills, Ollie Wilson\"	True story of notorious Australian outlaw Ned Kelly (1855-80).	6.1	589	\"$2,250 \"				7	7";
	list.add(s1);
	list.add(s2);
	list.add(s3);
	
	ImbdImporter testProcessor = new ImbdImporter("","",true);
	ArrayList<String> newList = new ArrayList<String>();
	newList = testProcessor.process(list);
	
	assertEquals("Name: Miss Jerry	Release Year: 1894	Runtime: 45	Source: IMBD",newList.get(0));
	assertEquals("Name: The Story of the Kelly Gang	Release Year: 1906	Runtime: 70	Source: IMBD",newList.get(1));
	}
	
}
	
	
